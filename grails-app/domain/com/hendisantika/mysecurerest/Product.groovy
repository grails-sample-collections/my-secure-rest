package com.hendisantika.mysecurerest

import grails.rest.Resource

@Resource(uri = '/api/product')
class Product {

    String prodName
    String prodDesc
    Double prodPrice
    Date createDate = new Date()

    static constraints = {
    }
}
